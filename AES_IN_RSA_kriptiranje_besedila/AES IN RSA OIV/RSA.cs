﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AES_IN_RSA_OIV
{
    class RSA
    {

        int dolzina = 0;
        public void DolzinaRsa(int x)
        {
            dolzina = x;
        }
        private static RSACryptoServiceProvider csp = new RSACryptoServiceProvider(2048);//tu das dolzino
        private RSAParameters _privateKey;
        private RSAParameters _publicKey;

        public RSA()
        {
            _privateKey = csp.ExportParameters(true);
            _publicKey = csp.ExportParameters(false);
            
        }

        public string PublicKeyString()
        {
            var sw = new StringWriter();
            var xs = new XmlSerializer(typeof(RSAParameters));
            xs.Serialize(sw, _publicKey);
            return sw.ToString();

        }

        public string Encrypt(string text1)
        {
            csp = new RSACryptoServiceProvider();
            csp.ImportParameters(_publicKey);

            var data = Encoding.Unicode.GetBytes(text1);
            var cypher = csp.Encrypt(data,false);
            return Convert.ToBase64String(cypher);  
        }

        public string Decrypt (string text2)
        {
            var dataBytes = Convert.FromBase64String(text2);
            csp.ImportParameters(_privateKey);
            var plaintext = csp.Decrypt(dataBytes, false);
            return Encoding.Unicode.GetString(plaintext);
        }
    }
}
