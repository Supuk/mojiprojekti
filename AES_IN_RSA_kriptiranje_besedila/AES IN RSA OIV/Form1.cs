﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AES_IN_RSA_OIV
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            obj_aes = new AES();
            obj_rsa = new RSA();
           Console.WriteLine( obj_rsa.PublicKeyString());
        }
        AES obj_aes;
        RSA obj_rsa;

        private void button1_Click(object sender, EventArgs e)
        {
            UTF8Encoding utf8 = new UTF8Encoding();
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;
                // MessageBox.Show(filename);

                string celo_besedilo = System.IO.File.ReadAllText(filename);
                Byte[] encodedBytes = utf8.GetBytes(celo_besedilo);
                for (int ctr = 0; ctr < encodedBytes.Length; ctr++)
                {
                    Console.Write("{0:X2} ", encodedBytes[ctr]);
                    if ((ctr + 1) % 25 == 0)
                        Console.WriteLine();
                }
                String decodedString = utf8.GetString(encodedBytes);
                textBox1.Text = decodedString;

            }
        }
        string celi_text1;
        string celi_text2;
        int dolzina = 0;

        private void button2_Click(object sender, EventArgs e)
        {
            celi_text1 = textBox1.Text;
          //  dolzina = Int32.Parse(textBoxDolzina.Text); //ta funkcionalnost ne deluje 

           // string rez = string.Empty;
            string x="";
            foreach (var item in checkedListBox1.CheckedItems)
            {
                     x =  item.ToString();
            }
           
            if (x== "AES")
            {
                obj_aes.DolzinaAes(dolzina);
                textBox2.Text = obj_aes.encrypt(celi_text1);

            }
            else
            {
                textBox2.Text = obj_rsa.Encrypt(celi_text1);

            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            celi_text2 = textBox2.Text;
           // string rez = string.Empty;
            string x = "";
            foreach (var item in checkedListBox1.CheckedItems)
            {
                x = item.ToString();
            }
            if (x == "AES")
            {
                textBox1.Text = obj_aes.decrypt(celi_text2);
            }
            else
            {
                textBox1.Text = obj_rsa.Decrypt(celi_text2);
            }
        }
    }
}
