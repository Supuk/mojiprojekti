﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace AES_IN_RSA_OIV
{
    class AES
    {
        AesCryptoServiceProvider crypt_provider;
        public AES()
        {   

            crypt_provider = new AesCryptoServiceProvider();
            crypt_provider.BlockSize = 128;
            crypt_provider.KeySize = 256; //dolžina oz izbira dolžine ključa 128;192 ali 256 
            crypt_provider.GenerateIV();
            crypt_provider.GenerateKey();
            crypt_provider.Mode = CipherMode.CBC;
            crypt_provider.Padding = PaddingMode.PKCS7;

        }
        int dolzina=0;
        public void DolzinaAes(int x)
        {
            dolzina = x;
        }

        public String encrypt(String text1)
        {
            ICryptoTransform transform = crypt_provider.CreateEncryptor();
            byte[] encrypted_bytes = transform.TransformFinalBlock(ASCIIEncoding.ASCII.GetBytes(text1), 0, text1.Length);
            string str = Convert.ToBase64String(encrypted_bytes);
            return str;

        }

        public String decrypt (String text2)
        {
            ICryptoTransform transform = crypt_provider.CreateDecryptor();
            byte[] enc_bytes = Convert.FromBase64String(text2);
            byte[] decrypted_bytes = transform.TransformFinalBlock(enc_bytes, 0, enc_bytes.Length);
            string str = ASCIIEncoding.ASCII.GetString(decrypted_bytes);
            return str;

        }
    }
}
