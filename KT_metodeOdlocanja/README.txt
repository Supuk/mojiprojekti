English:
Program for decision making based on KT methods. We enter different parameters and their weights.
The program does calculates the best option. It also draws 3 graphs for us.
The 3rd graph is a detailed view of one of the parameters which we select for a detailed view.
Written in C #.


Slovenian:
Program za odlocanje na podlagi KT metod. Vnesemo razlicne parametre in njihove utezi.
Program nam izracuna najboljso opcijo. Izrise nam tudi 3 grafe. 
3tji graf je podroben pogled enega izmed parametrov katerega izberemo za podroben ogled.
Napisano v C#.
