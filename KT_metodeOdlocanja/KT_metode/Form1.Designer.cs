﻿namespace KT_metode
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.gumb_korak1 = new System.Windows.Forms.Button();
            this.gumb_korak2 = new System.Windows.Forms.Button();
            this.gumb_dodajPar = new System.Windows.Forms.Label();
            this.gumb_dodajAtribute = new System.Windows.Forms.Label();
            this.lbl_vrednostAlter = new System.Windows.Forms.Label();
            this.gumb_dodajAlter = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lb_izračun = new System.Windows.Forms.Label();
            this.gumb_Izračun = new System.Windows.Forms.Button();
            this.lb_najboljsaAlternativa = new System.Windows.Forms.Label();
            this.txtBox_Izracun = new System.Windows.Forms.TextBox();
            this.graf = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.gumbGraf1 = new System.Windows.Forms.Button();
            this.gumbGraf2 = new System.Windows.Forms.Button();
            this.gumbGraf3 = new System.Windows.Forms.Button();
            this.graf2 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.graf3 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxIzbranParameter = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graf)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graf2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.graf3)).BeginInit();
            this.SuspendLayout();
            // 
            // gumb_korak1
            // 
            this.gumb_korak1.Location = new System.Drawing.Point(227, 63);
            this.gumb_korak1.Name = "gumb_korak1";
            this.gumb_korak1.Size = new System.Drawing.Size(116, 34);
            this.gumb_korak1.TabIndex = 0;
            this.gumb_korak1.Text = "Korak 1";
            this.gumb_korak1.UseVisualStyleBackColor = true;
            this.gumb_korak1.Click += new System.EventHandler(this.gumb_korak1_Click);
            // 
            // gumb_korak2
            // 
            this.gumb_korak2.Location = new System.Drawing.Point(227, 123);
            this.gumb_korak2.Name = "gumb_korak2";
            this.gumb_korak2.Size = new System.Drawing.Size(116, 30);
            this.gumb_korak2.TabIndex = 1;
            this.gumb_korak2.Text = "Korak 2";
            this.gumb_korak2.UseVisualStyleBackColor = true;
            this.gumb_korak2.Click += new System.EventHandler(this.gumb_korak2_Click);
            // 
            // gumb_dodajPar
            // 
            this.gumb_dodajPar.AutoSize = true;
            this.gumb_dodajPar.Location = new System.Drawing.Point(34, 72);
            this.gumb_dodajPar.Name = "gumb_dodajPar";
            this.gumb_dodajPar.Size = new System.Drawing.Size(160, 17);
            this.gumb_dodajPar.TabIndex = 2;
            this.gumb_dodajPar.Text = "Dodaj parameter in utež";
            this.gumb_dodajPar.Click += new System.EventHandler(this.label1_Click);
            // 
            // gumb_dodajAtribute
            // 
            this.gumb_dodajAtribute.AutoSize = true;
            this.gumb_dodajAtribute.Location = new System.Drawing.Point(34, 130);
            this.gumb_dodajAtribute.Name = "gumb_dodajAtribute";
            this.gumb_dodajAtribute.Size = new System.Drawing.Size(115, 17);
            this.gumb_dodajAtribute.TabIndex = 3;
            this.gumb_dodajAtribute.Text = "Dodaj alternative";
            this.gumb_dodajAtribute.Click += new System.EventHandler(this.label2_Click);
            // 
            // lbl_vrednostAlter
            // 
            this.lbl_vrednostAlter.AutoSize = true;
            this.lbl_vrednostAlter.Location = new System.Drawing.Point(38, 187);
            this.lbl_vrednostAlter.Name = "lbl_vrednostAlter";
            this.lbl_vrednostAlter.Size = new System.Drawing.Size(166, 17);
            this.lbl_vrednostAlter.TabIndex = 4;
            this.lbl_vrednostAlter.Text = "Dodaj vrednost alternativ";
            this.lbl_vrednostAlter.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // gumb_dodajAlter
            // 
            this.gumb_dodajAlter.Location = new System.Drawing.Point(227, 180);
            this.gumb_dodajAlter.Name = "gumb_dodajAlter";
            this.gumb_dodajAlter.Size = new System.Drawing.Size(111, 32);
            this.gumb_dodajAlter.TabIndex = 5;
            this.gumb_dodajAlter.Text = "Korak 3";
            this.gumb_dodajAlter.UseVisualStyleBackColor = true;
            this.gumb_dodajAlter.Click += new System.EventHandler(this.gumb_dodajAlter_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(900, 28);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(327, 119);
            this.dataGridView1.TabIndex = 6;
            // 
            // lb_izračun
            // 
            this.lb_izračun.AutoSize = true;
            this.lb_izračun.Location = new System.Drawing.Point(502, 72);
            this.lb_izračun.Name = "lb_izračun";
            this.lb_izračun.Size = new System.Drawing.Size(123, 17);
            this.lb_izračun.TabIndex = 7;
            this.lb_izračun.Text = "Pritisni za izračun ";
            // 
            // gumb_Izračun
            // 
            this.gumb_Izračun.Location = new System.Drawing.Point(647, 59);
            this.gumb_Izračun.Name = "gumb_Izračun";
            this.gumb_Izračun.Size = new System.Drawing.Size(122, 38);
            this.gumb_Izračun.TabIndex = 8;
            this.gumb_Izračun.Text = "Korak 4";
            this.gumb_Izračun.UseVisualStyleBackColor = true;
            this.gumb_Izračun.Click += new System.EventHandler(this.gumb_Izračun_Click);
            // 
            // lb_najboljsaAlternativa
            // 
            this.lb_najboljsaAlternativa.AutoSize = true;
            this.lb_najboljsaAlternativa.Location = new System.Drawing.Point(485, 161);
            this.lb_najboljsaAlternativa.Name = "lb_najboljsaAlternativa";
            this.lb_najboljsaAlternativa.Size = new System.Drawing.Size(140, 17);
            this.lb_najboljsaAlternativa.TabIndex = 9;
            this.lb_najboljsaAlternativa.Text = "Najboljša alternativa:";
            // 
            // txtBox_Izracun
            // 
            this.txtBox_Izracun.Location = new System.Drawing.Point(647, 161);
            this.txtBox_Izracun.Name = "txtBox_Izracun";
            this.txtBox_Izracun.Size = new System.Drawing.Size(580, 22);
            this.txtBox_Izracun.TabIndex = 10;
            // 
            // graf
            // 
            chartArea1.Name = "ChartArea1";
            this.graf.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.graf.Legends.Add(legend1);
            this.graf.Location = new System.Drawing.Point(37, 300);
            this.graf.Name = "graf";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.graf.Series.Add(series1);
            this.graf.Size = new System.Drawing.Size(525, 332);
            this.graf.TabIndex = 11;
            this.graf.Text = "chart1";
            this.graf.Click += new System.EventHandler(this.graf_Click);
            // 
            // gumbGraf1
            // 
            this.gumbGraf1.Location = new System.Drawing.Point(37, 259);
            this.gumbGraf1.Name = "gumbGraf1";
            this.gumbGraf1.Size = new System.Drawing.Size(98, 35);
            this.gumbGraf1.TabIndex = 12;
            this.gumbGraf1.Text = "Graf 1";
            this.gumbGraf1.UseVisualStyleBackColor = true;
            this.gumbGraf1.Click += new System.EventHandler(this.gumbGraf1_Click);
            // 
            // gumbGraf2
            // 
            this.gumbGraf2.Location = new System.Drawing.Point(647, 259);
            this.gumbGraf2.Name = "gumbGraf2";
            this.gumbGraf2.Size = new System.Drawing.Size(98, 35);
            this.gumbGraf2.TabIndex = 13;
            this.gumbGraf2.Text = "Graf 2";
            this.gumbGraf2.UseVisualStyleBackColor = true;
            this.gumbGraf2.Click += new System.EventHandler(this.gumbGraf2_Click);
            // 
            // gumbGraf3
            // 
            this.gumbGraf3.Location = new System.Drawing.Point(1256, 259);
            this.gumbGraf3.Name = "gumbGraf3";
            this.gumbGraf3.Size = new System.Drawing.Size(98, 35);
            this.gumbGraf3.TabIndex = 14;
            this.gumbGraf3.Text = "Graf 3";
            this.gumbGraf3.UseVisualStyleBackColor = true;
            this.gumbGraf3.Click += new System.EventHandler(this.gumbGraf3_Click);
            // 
            // graf2
            // 
            chartArea2.Name = "ChartArea1";
            this.graf2.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.graf2.Legends.Add(legend2);
            this.graf2.Location = new System.Drawing.Point(613, 300);
            this.graf2.Name = "graf2";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.graf2.Series.Add(series2);
            this.graf2.Size = new System.Drawing.Size(588, 326);
            this.graf2.TabIndex = 15;
            this.graf2.Text = "chart1";
            // 
            // graf3
            // 
            chartArea3.Name = "ChartArea1";
            this.graf3.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.graf3.Legends.Add(legend3);
            this.graf3.Location = new System.Drawing.Point(1256, 300);
            this.graf3.Name = "graf3";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "Series1";
            this.graf3.Series.Add(series3);
            this.graf3.Size = new System.Drawing.Size(588, 326);
            this.graf3.TabIndex = 16;
            this.graf3.Text = "chart1";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(1482, 81);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(216, 174);
            this.checkedListBox1.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1317, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(569, 17);
            this.label1.TabIndex = 18;
            this.label1.Text = "Izberi en parameter ki bi ga rad podrobno analiziral ter pritisni gumb za potrdit" +
    "ev in graf 3";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1320, 81);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 27);
            this.button1.TabIndex = 19;
            this.button1.Text = "Izberi Parameter";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(1721, 228);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(145, 27);
            this.button2.TabIndex = 20;
            this.button2.Text = "Potrdi izbiro";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBoxIzbranParameter
            // 
            this.textBoxIzbranParameter.Location = new System.Drawing.Point(1387, 265);
            this.textBoxIzbranParameter.Name = "textBoxIzbranParameter";
            this.textBoxIzbranParameter.Size = new System.Drawing.Size(311, 22);
            this.textBoxIzbranParameter.TabIndex = 21;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1924, 750);
            this.Controls.Add(this.textBoxIzbranParameter);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.graf3);
            this.Controls.Add(this.graf2);
            this.Controls.Add(this.gumbGraf3);
            this.Controls.Add(this.gumbGraf2);
            this.Controls.Add(this.gumbGraf1);
            this.Controls.Add(this.graf);
            this.Controls.Add(this.txtBox_Izracun);
            this.Controls.Add(this.lb_najboljsaAlternativa);
            this.Controls.Add(this.gumb_Izračun);
            this.Controls.Add(this.lb_izračun);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.gumb_dodajAlter);
            this.Controls.Add(this.lbl_vrednostAlter);
            this.Controls.Add(this.gumb_dodajAtribute);
            this.Controls.Add(this.gumb_dodajPar);
            this.Controls.Add(this.gumb_korak2);
            this.Controls.Add(this.gumb_korak1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.graf)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.graf2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.graf3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button gumb_korak1;
        private System.Windows.Forms.Button gumb_korak2;
        private System.Windows.Forms.Label gumb_dodajPar;
        private System.Windows.Forms.Label gumb_dodajAtribute;
        private System.Windows.Forms.Label lbl_vrednostAlter;
        private System.Windows.Forms.Button gumb_dodajAlter;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label lb_izračun;
        private System.Windows.Forms.Button gumb_Izračun;
        private System.Windows.Forms.Label lb_najboljsaAlternativa;
        private System.Windows.Forms.TextBox txtBox_Izracun;
        private System.Windows.Forms.DataVisualization.Charting.Chart graf;
        private System.Windows.Forms.Button gumbGraf1;
        private System.Windows.Forms.Button gumbGraf2;
        private System.Windows.Forms.Button gumbGraf3;
        private System.Windows.Forms.DataVisualization.Charting.Chart graf2;
        private System.Windows.Forms.DataVisualization.Charting.Chart graf3;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxIzbranParameter;
    }
}

