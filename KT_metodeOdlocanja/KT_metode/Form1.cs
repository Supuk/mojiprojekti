﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace KT_metode
{
    public partial class Form1 : Form
    {

        List<string> ParametriList = new List<string>();
        List<string> ParaUtezList = new List<string>();
        List<string> ParaUtezListZAIzracunNIC = new List<string>();
        List<string> ParaUtezListZAIzracunDESET = new List<string>();
        List<string> AlternativaList = new List<string>();
        List<string> VrednostAlternativeList = new List<string>();
        List<int> Izracun = new List<int>();
        List<int> IzracunZANIC = new List<int>();
        List<int> IzracunZADESET = new List<int>();
        DataTable zaDataView = new DataTable();

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void gumb_korak1_Click(object sender, EventArgs e)
        {


            string parameter = Interaction.InputBox("Vnesi parameter", "Vnos parametra", "", -1, -1);
            if (parameter != "")
            {
                string utez = Interaction.InputBox("Vnesi utež (0-10) za parameter: " + parameter, "Vnos uteži", "", -1, -1);
                if (Int32.Parse(utez) == 0 || Int32.Parse(utez) == 1 || Int32.Parse(utez) == 2 || Int32.Parse(utez) == 3 || Int32.Parse(utez) == 4 || Int32.Parse(utez) == 5 || Int32.Parse(utez) == 6 || Int32.Parse(utez) == 7 || Int32.Parse(utez) == 8 || Int32.Parse(utez) == 9 || Int32.Parse(utez) == 10)
                {
                    ParametriList.Add(parameter);
                    ParaUtezList.Add(utez);
                    MessageBox.Show("Parameter: " + parameter + "  in utež: " + utez + " USPEŠNO DODANA :)");
                    /* for (int i = 0; i < ParametriList.Count; i++)
                     {
                        //izpis zafrkava
                         zaDataView.Rows.Add(ParametriList.ElementAt(i));
                         zaDataView.Rows.Add(ParaUtezList.ElementAt(i));

                     }*/

                }
                else
                {
                    MessageBox.Show("Niste vnesli pravilne uteži");
                }
            }
            else
            {
                MessageBox.Show("Niste vnesli parametera");
            }
        }

        private void gumb_korak2_Click(object sender, EventArgs e)
        {
            string alternativa = Interaction.InputBox("Vnesi alternativo", "Vnos alternative", "", -1, -1);
            if (alternativa != "")
            {
                AlternativaList.Add(alternativa);
                MessageBox.Show("Alternativa: " + alternativa + " USPEŠNO DODANA :)");
            }
            else
            {
                MessageBox.Show("Niste vnesli alternative");

            }
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void gumb_dodajAlter_Click(object sender, EventArgs e)
        {
            for (int x = 0; x < AlternativaList.Count; x++)
            {

                for (int i = 0; i < ParametriList.Count; i++) //tu shrani vrednostiAlternativ v list pol pa z dolžino par, loči ok
                {
                    string vredostAlter = Interaction.InputBox("Vnesi vrednost alternative:" + AlternativaList.ElementAt(x) + " za parameter: " + ParametriList.ElementAt(i), "Vnos vrednosti alternative", "", -1, -1);
                    VrednostAlternativeList.Add(vredostAlter);
                }
            }
        }

        private void gumb_Izračun_Click(object sender, EventArgs e)
        {

            int x = 0;
            int alternativaIzracun = 0;
            int najboljsa = 0;
            int index = 0;
            for (int i = 0; i <= VrednostAlternativeList.Count; i++)
            {
                int izracun;
                if (i == 0)
                {
                    izracun = Int32.Parse(VrednostAlternativeList.ElementAt(i)) * Int32.Parse(ParaUtezList.ElementAt(x));
                    alternativaIzracun = alternativaIzracun + izracun;

                }
                else
                {
                    if ((i % ParametriList.Count) == 0)
                    {
                        Izracun.Add(alternativaIzracun);
                        x = 0;
                        alternativaIzracun = 0;
                    }
                    else
                    {
                        izracun = Int32.Parse(VrednostAlternativeList.ElementAt(i)) * Int32.Parse(ParaUtezList.ElementAt(x));
                        alternativaIzracun = alternativaIzracun + izracun;
                    }

                }
                x++;

            }

            for (int j = 0; j < Izracun.Count; j++)
            {
                najboljsa = Izracun.ElementAt(0);

                if (najboljsa < Izracun.ElementAt(j))
                {
                    najboljsa = Izracun.ElementAt(j);
                    index = j;
                }

            }
            txtBox_Izracun.Text = "Najboljša alternativa je: " + AlternativaList.ElementAt(index) + " z izračunom: " + najboljsa;
            zaDataView.Columns.Add("Alternative");
            zaDataView.Columns.Add("Izracun za alternative");
            
            for (int i = 0; i < AlternativaList.Count; i++)
            {
                string[] row = new string[] { AlternativaList.ElementAt(i), Izracun.ElementAt(i).ToString() };
                // zaDataView.Rows.InsertAt(Convert.ToBase64String(Izracun.ElementAt(i)),i);
                zaDataView.Rows.Add(row);

            }
            
            

                dataGridView1.DataSource = zaDataView;
        }

        private void gumbGraf1_Click(object sender, EventArgs e)
        {

            graf.Titles.Add("Primerjava alternativ");
            graf.Series.Clear();
            graf.Legends.Clear();
          //  var graf1 = graf.ChartAreas[0];
            //   g.AxisX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            //graf1.AxisX.LabelStyle.Format = "";
            //graf1.AxisY.LabelStyle.Format = "";
            //graf1.AxisX.LabelStyle.IsEndLabelVisible = true;
            //graf1.AxisY.LabelStyle.IsEndLabelVisible = true;

            //  g.AxisX.Minimum = 0;
            //  g.AxisY.Minimum = 0;


           // graf1.AxisX.Interval = 1;
          //  graf.AxisY.Interval = 5;
           // graf.AxisX.Title = "Alternative";
           // graf.AxisY.Title = "vrednost alternativ";
            graf.Series.Add("Series1");
            //graf.Series[0].IsVisibleInLegend = false;
            for (int i = 0; i < Izracun.Count; i++)
            {
              
                //      graf.Series[(AlternativaList.ElementAt(i))].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column;

                // graf.Series[(AlternativaList.ElementAt(i))].Points.Add(AlternativaList.ElementAt(i), Izracun.ElementAt(i));
                graf.Series["Series1"].Points.AddXY(AlternativaList.ElementAt(i), Izracun.ElementAt(i));

            }

        }

        private void gumbGraf2_Click(object sender, EventArgs e)
        {
            graf2.Series.Clear();
            graf2.Legends.Clear();
            graf2.Titles.Add("Parametri");

            // var graf = graf2.ChartAreas[0];

            //   g.AxisX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            // graf2.AxisX.LabelStyle.Format = "";
            // graf2.AxisY.LabelStyle.Format = "";
            // graf2.AxisX.LabelStyle.IsEndLabelVisible = true;
            // graf2.AxisY.LabelStyle.IsEndLabelVisible = true;

            //  g.AxisX.Minimum = 0;
            //  g.AxisY.Minimum = 0;


            /*   graf2.AxisX.Interval = 1;
               graf2.AxisY.Interval = 5;
               graf2.AxisX.Title = "Alternative";
               graf2.AxisY.Title = "vrednost alternativ";
               */
            graf2.Series.Add("Series1");
            graf2.Series["Series1"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Pie;

            for (int i = 0; i < ParametriList.Count; i++)
            {
             
                // graf.Series[(AlternativaList.ElementAt(i))].Points.Add(AlternativaList.ElementAt(i), Izracun.ElementAt(i));
                graf2.Series["Series1"].Points.AddXY(ParametriList.ElementAt(i), Int32.Parse( ParaUtezList.ElementAt(i)));

            }
        }

        private void gumbGraf3_Click(object sender, EventArgs e)
        {
            //graf3.Series.Clear();
           // graf3.Legends.Clear();
         
            var graf = graf3.ChartAreas[0];
            graf.AxisX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            graf.AxisX.LabelStyle.Format = "";
            graf.AxisY.LabelStyle.Format = "";
            graf.AxisX.LabelStyle.IsEndLabelVisible = true;
            graf.AxisY.LabelStyle.IsEndLabelVisible = true;

            graf.AxisX.Minimum = 0;
            graf.AxisY.Minimum = 0;
            graf.AxisX.Maximum = 10;

            graf.AxisX.Interval = 1;
            graf.AxisY.Interval = 10;
            graf.AxisX.Title = "Utež";
            graf.AxisY.Title = "Vrednosti";
            
            // graf.Series[0].IsVisibleInLegend = false;
            //ZA NIC IZRACUN
            for (int i = 0; i < ParaUtezList.Count; i++)
            {

                if (i == prvi)
                {
                    ParaUtezListZAIzracunNIC.Add("0");

                }
                else
                { 
                    ParaUtezListZAIzracunNIC.Add(ParaUtezList.ElementAt(i));
                }
            }


            {
                int x = 0;
                int alternativaIzracun = 0;
                int najboljsa = 0;
                int index = 0;
                for (int i = 0; i <= VrednostAlternativeList.Count; i++)
                {
                    int izracun;
                    if (i == 0)
                    {
                        izracun = Int32.Parse(VrednostAlternativeList.ElementAt(i)) * Int32.Parse(ParaUtezListZAIzracunNIC.ElementAt(x));
                        alternativaIzracun = alternativaIzracun + izracun;

                    }
                    else
                    {
                        if ((i % ParametriList.Count) == 0)
                        {
                            IzracunZANIC.Add(alternativaIzracun);
                            x = 0;
                            alternativaIzracun = 0;
                        }
                        else
                        {
                            izracun = Int32.Parse(VrednostAlternativeList.ElementAt(i)) * Int32.Parse(ParaUtezListZAIzracunNIC.ElementAt(x));
                            alternativaIzracun = alternativaIzracun + izracun;
                        }

                    }
                    x++;

                }
            }

            // ZA DESET
            for (int i = 0; i < ParaUtezList.Count; i++)
            {

                if (i == prvi)
                {
                    ParaUtezListZAIzracunDESET.Add("10");

                }
                else
                {
                    ParaUtezListZAIzracunDESET.Add(ParaUtezList.ElementAt(i));
                }
            }

            {
                int x = 0;
                int alternativaIzracun = 0;
                int najboljsa = 0;
                int index = 0;
                for (int i = 0; i <= VrednostAlternativeList.Count; i++)
                {
                    int izracun;
                    if (i == 0)
                    {
                        izracun = Int32.Parse(VrednostAlternativeList.ElementAt(i)) * Int32.Parse(ParaUtezListZAIzracunDESET.ElementAt(x));
                        alternativaIzracun = alternativaIzracun + izracun;

                    }
                    else
                    {
                        if ((i % ParametriList.Count) == 0)
                        {
                            IzracunZADESET.Add(alternativaIzracun);
                            x = 0;
                            alternativaIzracun = 0;
                        }
                        else
                        {
                            izracun = Int32.Parse(VrednostAlternativeList.ElementAt(i)) * Int32.Parse(ParaUtezListZAIzracunDESET.ElementAt(x));
                            alternativaIzracun = alternativaIzracun + izracun;
                        }

                    }
                    x++;

                }
            }


            for (int i = 0; i < AlternativaList.Count; i++)
            {

                graf3.Series.Add(AlternativaList.ElementAt(i));
                graf3.Series[(AlternativaList.ElementAt(i))].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

                graf3.Series[(AlternativaList.ElementAt(i))].Points.AddXY(0, IzracunZANIC.ElementAt(i));
                graf3.Series[(AlternativaList.ElementAt(i))].Points.AddXY(10, IzracunZADESET.ElementAt(i));


            }
        }

        private void graf_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            checkedListBox1.Items.Clear();
            foreach(string s in ParametriList)
            {
                checkedListBox1.Items.Add(s);
            }
        }
        int prvi;
        private void button2_Click(object sender, EventArgs e)
        {

            for (int i = 0; i < checkedListBox1.CheckedIndices.Count; i++)
            {
                prvi = checkedListBox1.CheckedIndices[i];

            }
            textBoxIzbranParameter.Text = "Izbran je naslednji paramter: " + ParametriList.ElementAt(prvi);
            
            
        }

        
    }
}
