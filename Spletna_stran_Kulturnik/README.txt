English:
The Kulturnik website is intended for viewing all cultural events in Slovenia.
From cinema to music events, sports, etc. The password for the admin page of this website is:zeloskrivnostnogeslo 
The website is made in the SpringBoot project at Intelij.
It also has RSS from existing web portals and the site uses XML to collect information about the latest movies of their pictures and descriptions.
All this is also displayed by clicking and signing on the admin page. We can browse and filter results by city, price, etc.
I did the project together with my classmates.

Slovenian:
Spletna stran Kulturnik je namenjena pogledu vseh kulturnih dogodkov v Sloveniji.
Od kina do glasbenih dogodkov, sportnih itd. Geslo za admin strani v spletni strani je: zeloskrivnostnogeslo
Spletna stran je narejena v Spring projektu v Intelij-u. 
Ima tudi rss in sicer z obstojecih spletnih portalov in stran s pomocjo XMLjev pobere informacije o najnovejih filmih njihovih slikah in opisih.
To vse se tudi prikaze s klikom na admin straneh. Lahko brskamo in filtriramo zadetke po mestih, ceni itd. 
Projekt sem naredil s sosolci skupaj.
