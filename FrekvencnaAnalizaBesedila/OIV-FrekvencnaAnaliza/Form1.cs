﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace OIV_FrekvencnaAnaliza
{
    public partial class Form1 : Form
    {

        DataTable moja_Baza_ref = new DataTable();
        DataTable moja_Baza_sif = new DataTable();
        List<int> stCrkRef = new List<int>();
        List<int> stCrkSif = new List<int>();
        List<int> rezultatSif = new List<int>();
        List<int> rezultatRef = new List<int>();
        public Form1()
        {
            InitializeComponent();
        }
        string celi_text_ref;
        string celi_text_sif;
        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;
                // MessageBox.Show(filename);

                ShraninPrikazi(filename);
                label1.Text = "Uspešno si naložil CSV";
            }
        }
        private void ShraninPrikazi(string filename)
        {
            string[] celi_text = System.IO.File.ReadAllLines(filename);
            string celo_besedilo_ref = System.IO.File.ReadAllText(filename);
            celi_text_ref = celo_besedilo_ref;
            moja_Baza_ref.Columns.Add("Besedilo");  
            foreach(string s in celi_text)
            {
                moja_Baza_ref.Rows.Add(s);
            }

            dataGridView1.DataSource = moja_Baza_ref;
            this.Controls.Add(dataGridView1);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;
                // MessageBox.Show(filename);

                ShraninPrikazi2(filename);
                label1.Text = "Uspešno si naložil CSV";
            }

        }
        private void ShraninPrikazi2(string filename)
        {
            string[] celi_text = System.IO.File.ReadAllLines(filename);
           string celo_besedilo_sif = System.IO.File.ReadAllText(filename);
            celi_text_sif = celo_besedilo_sif;

            moja_Baza_sif.Columns.Add("Besedilo");
            foreach (string s in celi_text)
            {
                moja_Baza_sif.Rows.Add(s);
            }

            dataGridView2.DataSource = moja_Baza_sif;
            this.Controls.Add(dataGridView2);
       
        }

        public static int stejCrko(string s, char c)
        {
            int res = 0;

            for (int i = 0; i < s.Length; i++)
            {

                
                if (s[i] == c)
                    res++;
            }

            return res;
        }
        private void button3_Click(object sender, EventArgs e)
        {

            string text1 = celi_text_ref;
            string text2 = celi_text_sif;
           
            string[] crke = { "a", "b", "c", "č", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "š", "t", "u", "v", "z", "ž" };
           
            for(int i = 0; i < crke.Length; i++)
            {
                stCrkRef.Add(stejCrko(text1, char.Parse(crke[i])));
                stCrkSif.Add(stejCrko(text2, char.Parse(crke[i])));
                //tu mamo zaj vse crke za text1 in text2

            }
            for (int i = 0; i < stCrkSif.Count; i++)
            {
                for (int j = 0; j < stCrkRef.Count; j++)    {  //mogo bi porihtat ce se podvojijo ker se lahko samo nism
                    if (stCrkSif.ElementAt(i) == stCrkRef.ElementAt(j))
                    {   //najdemo kje so enake cifre in potem shranimo pri kateri crki po vrsti se to zgodi za sifrirano in pol ref
                       // for (int z = 0; z < rezultatSif.Count; z++) {
                       //     if (rezultatSif.ElementAt(z) == i)
                      //          continue;
                      //      else
                                rezultatSif.Add(i);
                            rezultatRef.Add(j);
                            break;//ta break nam prepreči podvajanje
                    
                    }
                }
            }
            string text_sifriran_odsifriran ="";
           
            for (int i = 0; i < rezultatSif.Count; i++)
            {
                if (i == 0)
                {
                    text_sifriran_odsifriran = text2.Replace(crke[rezultatSif.ElementAt(i)], crke[rezultatRef.ElementAt(i)]);
                }
                else
                {
                    text_sifriran_odsifriran = text_sifriran_odsifriran.Replace(char.Parse(crke[rezultatSif.ElementAt(i)]), char.Parse(crke[rezultatRef.ElementAt(i)]));
                }
            }
            textBoxKoncnoBesedilo.Text = text_sifriran_odsifriran;
            spremenjen_text = text_sifriran_odsifriran;
          



        }

        string spremenjen_text = "";

        private void button4_Click(object sender, EventArgs e)
        {
            char crka1 = char.Parse(Interaction.InputBox("Vnesi katero črko bi želel zamenjat:", "Vnesi katero črko bi želel zamenjat:", "", -1, -1));
            char crka2 = char.Parse(Interaction.InputBox("Vnesi s katero črko bi želel zamenjat:", "Vnesi s katero črko bi želel zamenjat:", "", -1, -1));
            //spremenjen_text = spremenjen_text.Replace(crka2, crka1);
            spremenjen_text = spremenjen_text.Replace(crka1, crka2);
          //  spremenjen_text = spremenjen_text.Replace(crka2, crka1);
            //textBoxKoncnoBesedilo.Text = "";
            textBoxKoncnoBesedilo.Text = spremenjen_text;

        }

        private void button5_Click(object sender, EventArgs e)
        {
            string besedilo1 =(Interaction.InputBox("Vnesi katero besedo bi želel zamenjat:", "Vnesi katero besedo bi želel zamenjat:", "", -1, -1));
            string besedilo2 =(Interaction.InputBox("Vnesi s katero besedo bi želel zamenjat:", "Vnesi s katero besedo bi želel zamenjat:", "", -1, -1));
           // spremenjen_text = spremenjen_text.Replace(besedilo2, besedilo1);
            spremenjen_text = spremenjen_text.Replace(besedilo1, besedilo2);
            textBoxKoncnoBesedilo.Text = spremenjen_text;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            System.IO.File.WriteAllText(@"E:\OIV VAJE\WriteText.txt", spremenjen_text);
        }
    }
}
