English:
This program checks and performs Frequency analysis of encrypted text and helps us decipher the text.
We can also replace individual characters in the text. It is written in C #.
The test text is in the test text folder. Enter the reference and ciphertext.

Slovenian:
Ta program preveri in izvede Frekvencno analizo sifriranega besedila in nam pomaga desifrirat besedilo.
Lahko tudi zamenjamo posamezne znake v besedilu. Napisan je v C#. 
Testno besedilo je v mapi testnoBesedilo. Vnesemo referencno in sifrirano besedilo.
