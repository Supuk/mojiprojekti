﻿namespace ProjektMOS
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelNavodila = new System.Windows.Forms.Label();
            this.labelIzberi = new System.Windows.Forms.Label();
            this.CSVgumb = new System.Windows.Forms.Button();
            this.dataGridViewCSV = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridViewTestna = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridViewMatrika = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtBoxTocnost = new System.Windows.Forms.TextBox();
            this.txtBoxFscore = new System.Windows.Forms.TextBox();
            this.txtBoxPrecision = new System.Windows.Forms.TextBox();
            this.txtBoxRecall = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCSV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTestna)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMatrika)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNavodila
            // 
            this.labelNavodila.AutoSize = true;
            this.labelNavodila.Location = new System.Drawing.Point(12, 9);
            this.labelNavodila.Name = "labelNavodila";
            this.labelNavodila.Size = new System.Drawing.Size(666, 17);
            this.labelNavodila.TabIndex = 1;
            this.labelNavodila.Text = "Ta aplikacija prebere CSV datoteko in na podlagi tega nardi po klasifikacijskem a" +
    "lgoritmu matriko zmede.";
            this.labelNavodila.Click += new System.EventHandler(this.labelNavodila_Click);
            // 
            // labelIzberi
            // 
            this.labelIzberi.AutoSize = true;
            this.labelIzberi.Location = new System.Drawing.Point(27, 77);
            this.labelIzberi.Name = "labelIzberi";
            this.labelIzberi.Size = new System.Drawing.Size(167, 17);
            this.labelIzberi.TabIndex = 2;
            this.labelIzberi.Text = "Izberi učno CSV datoteko";
            // 
            // CSVgumb
            // 
            this.CSVgumb.BackColor = System.Drawing.Color.Aqua;
            this.CSVgumb.Location = new System.Drawing.Point(207, 64);
            this.CSVgumb.Name = "CSVgumb";
            this.CSVgumb.Size = new System.Drawing.Size(110, 42);
            this.CSVgumb.TabIndex = 3;
            this.CSVgumb.Text = "CSV datoteka";
            this.CSVgumb.UseVisualStyleBackColor = false;
            this.CSVgumb.Click += new System.EventHandler(this.CSVgumb_Click);
            // 
            // dataGridViewCSV
            // 
            this.dataGridViewCSV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCSV.Location = new System.Drawing.Point(30, 129);
            this.dataGridViewCSV.Name = "dataGridViewCSV";
            this.dataGridViewCSV.RowHeadersWidth = 51;
            this.dataGridViewCSV.RowTemplate.Height = 24;
            this.dataGridViewCSV.Size = new System.Drawing.Size(702, 237);
            this.dataGridViewCSV.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 405);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(175, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Izberi testno CSV datoteko";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Aqua;
            this.button1.Location = new System.Drawing.Point(207, 392);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 42);
            this.button1.TabIndex = 6;
            this.button1.Text = "CSV datoteka";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridViewTestna
            // 
            this.dataGridViewTestna.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewTestna.Location = new System.Drawing.Point(30, 452);
            this.dataGridViewTestna.Name = "dataGridViewTestna";
            this.dataGridViewTestna.RowHeadersWidth = 51;
            this.dataGridViewTestna.RowTemplate.Height = 24;
            this.dataGridViewTestna.Size = new System.Drawing.Size(702, 233);
            this.dataGridViewTestna.TabIndex = 7;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Lime;
            this.button2.Location = new System.Drawing.Point(38, 706);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(206, 35);
            this.button2.TabIndex = 8;
            this.button2.Text = "Potrdi vnos";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(931, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 17);
            this.label2.TabIndex = 9;
            this.label2.Text = "Matrika zmede";
            // 
            // dataGridViewMatrika
            // 
            this.dataGridViewMatrika.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMatrika.Location = new System.Drawing.Point(768, 129);
            this.dataGridViewMatrika.Name = "dataGridViewMatrika";
            this.dataGridViewMatrika.RowHeadersWidth = 51;
            this.dataGridViewMatrika.RowTemplate.Height = 24;
            this.dataGridViewMatrika.Size = new System.Drawing.Size(478, 207);
            this.dataGridViewMatrika.TabIndex = 10;
            this.dataGridViewMatrika.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(753, 427);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 17);
            this.label3.TabIndex = 11;
            this.label3.Text = "Metrike:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(753, 478);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 17);
            this.label4.TabIndex = 12;
            this.label4.Text = "Točnost:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(753, 519);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 17);
            this.label5.TabIndex = 13;
            this.label5.Text = "F-score:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(753, 563);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Precision:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(753, 608);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 17);
            this.label7.TabIndex = 15;
            this.label7.Text = "Recall:";
            // 
            // txtBoxTocnost
            // 
            this.txtBoxTocnost.Location = new System.Drawing.Point(845, 475);
            this.txtBoxTocnost.Name = "txtBoxTocnost";
            this.txtBoxTocnost.Size = new System.Drawing.Size(217, 22);
            this.txtBoxTocnost.TabIndex = 16;
            // 
            // txtBoxFscore
            // 
            this.txtBoxFscore.Location = new System.Drawing.Point(845, 519);
            this.txtBoxFscore.Name = "txtBoxFscore";
            this.txtBoxFscore.Size = new System.Drawing.Size(217, 22);
            this.txtBoxFscore.TabIndex = 17;
            // 
            // txtBoxPrecision
            // 
            this.txtBoxPrecision.Location = new System.Drawing.Point(845, 563);
            this.txtBoxPrecision.Name = "txtBoxPrecision";
            this.txtBoxPrecision.Size = new System.Drawing.Size(217, 22);
            this.txtBoxPrecision.TabIndex = 18;
            // 
            // txtBoxRecall
            // 
            this.txtBoxRecall.Location = new System.Drawing.Point(845, 608);
            this.txtBoxRecall.Name = "txtBoxRecall";
            this.txtBoxRecall.Size = new System.Drawing.Size(217, 22);
            this.txtBoxRecall.TabIndex = 19;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1258, 755);
            this.Controls.Add(this.txtBoxRecall);
            this.Controls.Add(this.txtBoxPrecision);
            this.Controls.Add(this.txtBoxFscore);
            this.Controls.Add(this.txtBoxTocnost);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.dataGridViewMatrika);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.dataGridViewTestna);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridViewCSV);
            this.Controls.Add(this.CSVgumb);
            this.Controls.Add(this.labelIzberi);
            this.Controls.Add(this.labelNavodila);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCSV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewTestna)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMatrika)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNavodila;
        private System.Windows.Forms.Label labelIzberi;
        private System.Windows.Forms.Button CSVgumb;
        private System.Windows.Forms.DataGridView dataGridViewCSV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridViewTestna;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridViewMatrika;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtBoxTocnost;
        private System.Windows.Forms.TextBox txtBoxFscore;
        private System.Windows.Forms.TextBox txtBoxPrecision;
        private System.Windows.Forms.TextBox txtBoxRecall;
    }
}

