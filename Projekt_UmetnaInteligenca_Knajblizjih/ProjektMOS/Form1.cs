﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektMOS
{
    public partial class Form1 : Form

    {
        DataTable moja_Baza = new DataTable();
        DataTable moja_Baza_testna = new DataTable();
        DataTable matrika = new DataTable();
        List<Double> dolzine = new List<Double>();
        List<Double> dolzine1 = new List<Double>();
        List<String> razredi_za_testne = new List<String>();
        List<String> vsiRazredi = new List<String>();
        List<String> vrstice = new List<String>();
        List<int> pravilniZaVsiRazredi = new List<int>();
        List<double> recalcici = new List<double>();
        List<double> vsiProcenti = new List<double>();
        public Form1()
        {
            InitializeComponent();
        }

        private void labelNavodila_Click(object sender, EventArgs e)
        {

        }

        private void CSVgumb_Click(object sender, EventArgs e)
        {



            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;
                // MessageBox.Show(filename);

                ShraninPrikaziCSV(filename);
                label1.Text = "Uspešno si naložil CSV";
            }
        }


        private void ShraninPrikaziCSV(string filename)
        {
            string[] celi_text = System.IO.File.ReadAllLines(filename);
            string[] data_col = null;
            int x = 0;

            foreach (string text_line in celi_text)
            {
                data_col = text_line.Split(',');

                if (x == 0)
                {
                    // header
                    for (int i = 0; i <= data_col.Count() - 1; i++)
                    {
                        moja_Baza.Columns.Add(data_col[i]);
                    }
                    x++;
                }
                else
                {
                    moja_Baza.Rows.Add(data_col);
                    //podatki

                }

            }
            dataGridViewCSV.DataSource = moja_Baza;
            this.Controls.Add(dataGridViewCSV);
        }

        private void button1_Click(object sender, EventArgs e)
        {

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename2 = openFileDialog1.FileName;
                // MessageBox.Show(filename);

                ShraninPrikaziCSV2(filename2);
                labelIzberi.Text = "Uspešno si naložil CSV";
            }
        }
        private void ShraninPrikaziCSV2(string filename)
        {
            string[] celi_text = System.IO.File.ReadAllLines(filename);
            string[] data_col = null;
            int x = 0;

            foreach (string text_line in celi_text)
            {
                data_col = text_line.Split(',');

                if (x == 0)
                {
                    // header
                    for (int i = 0; i <= data_col.Count() - 1; i++)
                    {
                        moja_Baza_testna.Columns.Add(data_col[i]);
                    }
                    x++;
                }
                else
                {
                    moja_Baza_testna.Rows.Add(data_col);
                    //podatki

                }

            }
            dataGridViewTestna.DataSource = moja_Baza_testna;
            this.Controls.Add(dataGridViewTestna);
        }

        private double izracunajDolzino(int vrstica, int ucna){
            double dolzina=0;
            for (int i=0; i < moja_Baza.Columns.Count - 2; i++)
            {
                double x = Math.Pow(double.Parse(moja_Baza.Rows[ucna][i].ToString()) - double.Parse(moja_Baza_testna.Rows[vrstica][i].ToString()), 2);
                dolzina = dolzina + x;
            }
            return dolzina;

        }
        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < moja_Baza_testna.Rows.Count; i++)
            {

                for (int j = 0; j < moja_Baza.Rows.Count; j++)
                {



                    //zracunam dolzine za vse primere
                    double dolzina = Math.Sqrt(izracunajDolzino(i, j));

                    dolzine.Add(dolzina);//ta list mam samo da lažje sortiram
                    dolzine1.Add(dolzina);//dam v list vse dolzine
                }
                dolzine.Sort();
                double k1 = dolzine[0];
                double k2 = dolzine[1];
                double k3 = dolzine[2];
                int prviIndex = 0;
                int drugIndex = 0;
                int tretjiIndex = 0;

                for (int z = 0; z < dolzine1.Count; z++)  //to sem naredil da dobim index za razred da vem
                {
                    if (dolzine1[z] == k1)
                    {
                        prviIndex = z;

                    }
                    else if (dolzine1[z] == k2)
                    {
                        drugIndex = z;

                    }
                    else if (dolzine1[z] == k3)
                    {
                        tretjiIndex = z;

                    }
                }
                string razred1 = moja_Baza.Rows[prviIndex][moja_Baza.Columns.Count - 1].ToString();
                string razred2 = moja_Baza.Rows[drugIndex][moja_Baza.Columns.Count - 1].ToString();
                string razred3 = moja_Baza.Rows[tretjiIndex][moja_Baza.Columns.Count - 1].ToString();
                string razred;

                if (razred1 == razred2 || razred2 == razred3)
                {
                    razred = razred2;
                }
                else if (razred1 == razred3)
                {
                    razred = razred1;
                }
                else { razred = razred1; }     //v kolikor so vsi 3je razlicni je najbližjemu enak 

                razredi_za_testne.Add(razred); //dodam rezultate razredov v List
                                               // MessageBox.Show(razred);
                                               //za izpis matrike



                dolzine.Clear();
                dolzine1.Clear();
            }
            int stRazredov = 0;
            string x = moja_Baza_testna.Rows[0][moja_Baza_testna.Columns.Count - 1].ToString();
            vsiRazredi.Add("");
            vsiRazredi.Add(x);
            for (int i = 0; i < moja_Baza_testna.Rows.Count; i++)
            {


                string y = moja_Baza_testna.Rows[i][moja_Baza_testna.Columns.Count - 1].ToString();
                if (x == y)
                {

                }
                else
                {
                    x = y;
                    vsiRazredi.Add(x);
                    stRazredov = stRazredov + 1;
                }
            }
            for (int i = 0; i < vsiRazredi.Count; i++)
            {
                matrika.Columns.Add(vsiRazredi[i]);


            }


            for (int i = 1; i < matrika.Columns.Count; i++)
            {

                vrstice.Add(vsiRazredi[i]);
            }

          
           
            for (int j = 1; j < matrika.Columns.Count; j++)
            {
                int pravilni = 0;
                int narobe1 = 0;
                int narobe2 = 0;
                int narobe3 = 0;
                for (int i = 0; i < moja_Baza_testna.Rows.Count; i++)
                {
                    
                    if (vsiRazredi[j] == razredi_za_testne[i] && razredi_za_testne[i] == moja_Baza_testna.Rows[i][moja_Baza.Columns.Count-1].ToString())
                    {
                        pravilni++;
                    }else if
                        (vsiRazredi[j] == razredi_za_testne[i] && razredi_za_testne[i] != moja_Baza_testna.Rows[i][moja_Baza.Columns.Count - 1].ToString())
                    {   
                       
                        if(vsiRazredi[1] == moja_Baza_testna.Rows[i][moja_Baza.Columns.Count - 1].ToString())
                        {
                            narobe1++;
                        }
                        else if(vsiRazredi[2] == moja_Baza_testna.Rows[i][moja_Baza.Columns.Count - 1].ToString())
                        {
                            narobe2++;
                        }
                        else
                        {
                            narobe3++;
                        }
                    }
                }
               // MessageBox.Show(pravilni.ToString());
                if (j == 1) {
                    pravilniZaVsiRazredi.Add(pravilni);
                    pravilniZaVsiRazredi.Add(narobe2);
                    pravilniZaVsiRazredi.Add(narobe3);
                }else if (j == 2)
                {
                    pravilniZaVsiRazredi.Add(narobe1);
                    pravilniZaVsiRazredi.Add(pravilni);
                    pravilniZaVsiRazredi.Add(narobe3);
                }else if (j == 3)
                {
                    pravilniZaVsiRazredi.Add(narobe1);
                    pravilniZaVsiRazredi.Add(narobe2);
                    pravilniZaVsiRazredi.Add(pravilni);
                }
                
              
            }
            
            matrika.Rows.Add(new Object[]{
                vrstice.ElementAt(0),
                pravilniZaVsiRazredi.ElementAt(0),
                 pravilniZaVsiRazredi.ElementAt(1),
                  pravilniZaVsiRazredi.ElementAt(2)
                   });
            matrika.Rows.Add(new Object[]{
                vrstice.ElementAt(1),
                pravilniZaVsiRazredi.ElementAt(3),
                 pravilniZaVsiRazredi.ElementAt(4),
                  pravilniZaVsiRazredi.ElementAt(5)
                   });
            matrika.Rows.Add(new Object[]{
                vrstice.ElementAt(2),
                pravilniZaVsiRazredi.ElementAt(6),
                 pravilniZaVsiRazredi.ElementAt(7),
                  pravilniZaVsiRazredi.ElementAt(8)
                   });
           
            dataGridViewMatrika.DataSource = matrika;
            this.Controls.Add(dataGridViewMatrika);
            int krneke = 1;
            int lolek = 0;
            double vsiPozitivni=0;
            for (int i = 0; i < 3; i++)
            {
                
               
                   double pozitivni = double.Parse(matrika.Rows[lolek+i][krneke].ToString());
               
               
                krneke++;
                vsiPozitivni = vsiPozitivni + pozitivni;
            }
            double tocnost = vsiPozitivni / razredi_za_testne.Count;
          //  MessageBox.Show(razredi_za_testne.Count.ToString());
            txtBoxTocnost.Text = tocnost.ToString();
            double falsenegativni1 = 0;
            for (int i = 0; i < matrika.Rows.Count; i++)
            { for(int j = 1; j < matrika.Columns.Count; j++)
                {
                    if (i == 0) { break; }
                    if (i == 0 && j == 1 || i == 1 && j == 2 || i == 2 && j == 3 || i == 3 && j == 4)
                    {
                        continue;
                    }
                    
                  
                     double recalcic = double.Parse(matrika.Rows[i][j].ToString());
                   
                   

                    falsenegativni1 = falsenegativni1 + recalcic;
                    
                }

            }
            recalcici.Add(double.Parse(matrika.Rows[0][1].ToString())/(double.Parse(matrika.Rows[0][1].ToString())+falsenegativni1));
            double falsenegativni2 = 0;
            for (int i = 0; i < matrika.Rows.Count; i++)
            {
                for (int j = 1; j < matrika.Columns.Count; j++)
                {
                    if (i == 1) { break; }
                    if (i == 0 && j == 1 || i == 1 && j == 2 || i == 2 && j == 3 || i == 3 && j == 4)
                    {
                        continue;
                    }


                    double recalcic = double.Parse(matrika.Rows[i][j].ToString());



                    falsenegativni2 = falsenegativni2 + recalcic;

                }

            }
            recalcici.Add(double.Parse(matrika.Rows[1][2].ToString()) / (double.Parse(matrika.Rows[1][2].ToString()) + falsenegativni2));
            double falsenegativni3 = 0;
            for (int i = 0; i < matrika.Rows.Count; i++)
            {
                for (int j = 1; j < matrika.Columns.Count; j++)
                {
                    if (i == 2) { break; }
                    if (i == 0 && j == 1 || i == 1 && j == 2 || i == 2 && j == 3 || i == 3 && j == 4)
                    {
                        continue;
                    }


                    double recalcic = double.Parse(matrika.Rows[i][j].ToString());



                    falsenegativni3 = falsenegativni3 + recalcic;

                }

            }
            recalcici.Add(double.Parse(matrika.Rows[2][3].ToString()) / (double.Parse(matrika.Rows[2][3].ToString()) + falsenegativni3));

        
            for (int j = 1; j < vsiRazredi.Count; j++)
            {
                double procent = 0;
                for (int i = 0; i < moja_Baza.Rows.Count; i++)
                {
                    if (vsiRazredi[j] == moja_Baza.Rows[i][moja_Baza.Columns.Count - 1].ToString())
                    {
                        procent=procent +1;
                    }
                   
                }
               
                double racunProcenta = procent / (double.Parse(moja_Baza.Rows.Count.ToString()));
                
                vsiProcenti.Add(racunProcenta);
            }

            double skupniRecall = 0;
            for(int i = 0; i < recalcici.Count; i++)
            {
                skupniRecall = skupniRecall + (vsiProcenti[i] * recalcici[i]);
            }
            txtBoxRecall.Text = skupniRecall.ToString();
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }


}


