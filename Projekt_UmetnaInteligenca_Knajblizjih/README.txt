English:
This is a machine learning program that works on the principle of K closest neighbors (algorithm).
The program is written in C # in Visual Studio.
CSV test files are in the CSV test folder. We need to choose the same learning and test to work properly.
The program uses learning Csv file to learn and predicted on the test, confusion matrix is printed and a couple of parameters.

Slovenian:
To je program za strojno ucenje in sicer deluje na principu K najblizjih sosedov(algoritem).
Program je napisan v C# v Visual Studio-u. 
CSV datoteke za priskus so v mapi CSV test. Izbrati moramo isto ucno in testno da deluje pravilno.
Program iz ucne Csv datoteke se nauci in predvideva na testni, izpise matriko zmede in par parametrov.
