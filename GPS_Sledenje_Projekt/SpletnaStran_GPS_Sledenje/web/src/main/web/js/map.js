function setCookie(cname,cvalue,exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
var a = [];
function checkCookie() {
    var sledeni = getCookie("sledeni");
    if (sledeni != "") {
        a = JSON.parse(sledeni);
        var elem = document.getElementById('trackedList');
        var elemm;
        for(var i = 0; i < a.length; i++){
            console.log(a[i]);
            elemm = document.createElement('a');
            elemm.innerText = a[i];
            elemm.innerHTML= a[i] + '<i id="'+ a[i] + '" class="fa fa-times pull-right" onclick="deleteTracked(this)" aria-hidden="true"></i>';
            elemm.className = "collapse-item";
            elemm.id = a[i];
            elemm.setAttribute("onclick", "poisci(this)");
            elem.appendChild(elemm);
        }

    } else {
        var arr = [];
        var json_str = JSON.stringify(arr);
        setCookie("sledeni", json_str, 30);
        location.reload();
    }
}
function deleteTracked(iden) {
    ident = iden.id;
    var json_str = getCookie('sledeni');
    var a = JSON.parse(json_str);
    for (var i = 0; i < a.length; i++) {
        console.log('im in the loop');
        if(a[i] == ident) {
            a.splice(i, 1);
            json_str = JSON.stringify(a);
            setCookie("sledeni", json_str, 30);
            location.reload();
        }
    }


}
function getListFromDB() {
    var b = [];
    var IDlist = firebase.database().ref('UserLocation');
    IDlist.orderByChild("id").on("child_added", function(data) {
        b.push(data.val().id);
        console.log(data.val().id);
    });
    var loggedIDlist = firebase.database().ref('PrijavljenUser');
    loggedIDlist.orderByChild("id").on("child_added", function(data) {
        b.push(data.val().id);
        console.log(data.val().id);
    });
    return b;
}
var n = [];
function poisci(iden) {
    n = [];
    ident = iden.id;
    const dbRefObject = firebase.database().ref().child('UserLocation');

    dbRefObject.on('child_added', snap => {if(snap.val().id == ident) {
        addToPath(snap.val().sirina, snap.val().dolzina, snap.val().komentar)}
    });

    const dbRefObject2 = firebase.database().ref().child('PrijavljenUser');

    dbRefObject2.on('child_added', snap => {if(snap.val().id == ident) {
        addToPath(snap.val().sirina, snap.val().dolzina, snap.val().komentar)}
    });
}

function search() {
    n = [];
    var iden = document.getElementById('idSearch').value;

    const lokacijeNeprijavljenih = firebase.database().ref().child('UserLocation');

    lokacijeNeprijavljenih.on('child_added', snap => {if(snap.val().id == iden) {
        addToPath(snap.val().sirina, snap.val().dolzina, snap.val().komentar)}
    });

    const lokacijePrijavljenih = firebase.database().ref().child('PrijavljenUser');

    lokacijePrijavljenih.on('child_added', snap => {if(snap.val().id == iden) {
        addToPath(snap.val().sirina, snap.val().dolzina, snap.val().komentar)}
    });

    var json_str = getCookie('sledeni');
    var a = JSON.parse(json_str);

    var obstaja = false;
    for (var i = 0; i < a.length; i++) {
        if(a[i] == iden) {
            obstaja = true;
            break;
        }
    }

    var seznam = getListFromDB();
    console.log(seznam);

    if(obstaja == false){
        for (var p = 0; p < seznam.length; p++) {
            if(seznam[p] == iden) {
                console.log('sem tu');
                a.push(iden);
                json_str = JSON.stringify(a);
                setCookie("sledeni", json_str, 30);
                var elem = document.getElementById('trackedList');
                var elemm = document.createElement('a');
                elemm.innerText = iden;
                elemm.className = "collapse-item";
                elemm.id = iden;
                elemm.setAttribute("onclick", "poisci(this)");
                elemm.innerHTML= a[i] + '<i id="'+ a[i] + '" class="fa fa-times pull-right" onclick="deleteTracked(this)" aria-hidden="true"></i>';
                elem.appendChild(elemm);
                break;
            }
        }

    }
}

function mojaZgodovina(eemail) {
    var dates = [];
    var dateList = firebase.database().ref("PrijavljenUser");

    dateList.orderByChild("email").on("child_added", function(data) {
        if (data.val().email == eemail) {
            const date = new Date(data.val().datum);
            const dateTimeFormat = new Intl.DateTimeFormat('en', {year: 'numeric', month: 'numeric', day: '2-digit'})
            const [{value: month}, , {value: day}, , {value: year}] = dateTimeFormat.formatToParts(date);
            var newDate = `${day}-${month}-${year}`;
            var obstaja3 = false;
            for(var i = 0; i < dates.length; i++) {
                if(dates[i] == newDate){
                    obstaja3 = true;
                    return;
                }
            }
            if (obstaja3 == false) {
                dates.push(newDate);
                var el = document.getElementById('history');
                dates.push(newDate);
                var elt = document.createElement('a');
                elt.className = 'collapse-item';
                elt.innerText = `${day}-${month}-${year}`;
                elt.title = eemail;
                elt.id = `${day}-${month}-${year}`;
                elt.setAttribute("onclick", "showHistory(this)");
                el.appendChild(elt);
            }
        }
    });
}

function showHistory(d) {
    n = [];
    var sHistory = firebase.database().ref("PrijavljenUser");

    sHistory.orderByChild("email").on("child_added", function(data) {
        if (data.val().email == d.title) {
            const date = new Date(data.val().datum);
            const dateTimeFormat = new Intl.DateTimeFormat('en', {year: 'numeric', month: 'numeric', day: '2-digit'})
            const [{value: month}, , {value: day}, , {value: year}] = dateTimeFormat.formatToParts(date);
            var newDate = `${day}-${month}-${year}`;
            if(newDate == d.id) {
                addToPath(data.val().sirina, data.val().dolzina, data.val().komentar);
            }
        }
    });
}
function addToPath(lati, lngi, comment) {
    n.push({lat: parseFloat(lati), lng: parseFloat(lngi)});
    initMap(parseFloat(lati), parseFloat(lngi), comment);
}

var map;
function initMap(latitude, longitude, comment) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: parseFloat(latitude), lng: parseFloat(longitude)},
        zoom: 8
    });

    new google.maps.Marker({
        position:{lat: parseFloat(latitude), lng:parseFloat(longitude)},
        map:map,
        title: comment
    });

    var flightPath = new google.maps.Polyline({
        path: n,
        geodesic: true,
        strokeColor: '#FF0000',
        strokeOpacity: 1.0,
        strokeWeight: 2
    });

    flightPath.setMap(map);

}
