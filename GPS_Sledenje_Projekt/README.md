**GPS TRACKING**

The GPS Tracking project consists of a mobile and a web application.

**Web application:**

Web application idea:

The web application is intended to display data that users transmit with a mobile phone via a mobile application.

Web application functionalities:
- Monitor the location or routes of other users who broadcast the location using the mobile application.
- Registered users have the ability to view the history of their location broadcasts by day

Development environments and tools:
* IntelliJ IDEA
* Spring project
* Bootstrap
* JavaScript

The web application also uses Firebase Hosting and is published online. It is available at the following link:
https://test-7203c.web.app/

You only need to clone the repository to edit the project.

Link to website repository:
https://gitlab.com/Supuk/praktikum2

**Data base:**

The two applications are connected to the Firebase Realtime Database, which allows you to dynamically display the path of users in the web application.
Firebase applications serve as a data store and user authentication service.

**Mobile application**

Mobile app idea:

The mobile application is intended to broadcast the location or route of the user. In it, the user can log in with an existing account.
Or create a new one. The app also allows you to display your current location on a folder.

Mobile application functionalities:
- Rental location
- Realtime broadcast routes
- Display the submitted location on the map
- Generate a random ID


Development environments and tools:
* Android Studio
* Java
* Material Design
* XML

To use and run the application, you need to clone the repository and run it with Android Studio.


Slovenian:
**GPS SLEDENJE**

Projekt GPS Sledenje sestoji iz mobilne in spletne aplikacije.

**Spletna aplikacija:**

Ideja spletne aplikacije:

Spletna aplikacija je namenjena prikazovanju podatkov, ki jih uporabniki oddajajo z mobilnim telefonom preko mobilne aplikacije.

Funkcionalnosti spletne aplikacije:
- Spremljanje lokacije ali poti drugih uporabnikov, ki lokacijo oddajajo s pomočjo mobilne aplikacije.
- Registrirani uporabniki imajo možnost pregleda zgodovine svojih oddajanj lokacij po dnevih

Razvojna okolja in orodja:
* IntelliJ IDEA
* Spring projekt
* Bootstrap
* JavaScript

Spletna aplikacija uporablja tudi Firebase Hosting in je objavljena na spletu. Dostopna je na naslednji povezavi:
https://test-7203c.web.app/

Za urejanje projekta je potrebno samo klonirat repozitorij.

Povezava do repozitorija spletne strani:
https://gitlab.com/Supuk/praktikum2

**Podatkovna baza:**

Aplikaciji sta povezani na Firebase Realtime Database, ki omogoča dinamično prikazovanje poti uporabnikov v spletni aplikaciji.
Firebase aplikacijama služi kot podatkovna shramba in storitev za avtentikacijo uporabnikov.

**Mobilna aplikacija**

Ideja mobilne aplikacije:

Mobilna aplikacija je namenjena oddajanju lokacije ali poti uporabnika. V njej se uporabnik lahko prijavi z že obstoječim računom.
Ali pa si ustvari novega. Aplikacija omogoča tudi prikazovanje trenutne lokacije na mapi.

Funkcionalnosti mobilne aplikacije:
- Oddajanje lokacije
- Realtime oddajanje poti
- Prikazovanje oddane lokacije na mapi
- Generiranje random IDja


Razvojna okolja in orodja:
* Android Studio
* Java
* Material Design
* XML

Za uporabo in izvedbo aplikacije je potrebno repozitorij klonirat in z Android Studiom zagnat.

Povezava do repozitorija mobilne aplikacije:
https://gitlab.com/Supuk/androidstudioscene
