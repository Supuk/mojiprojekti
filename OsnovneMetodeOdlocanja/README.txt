English:
Program for basic decision making methods.
It reads data from a CSV file and calculates it for basic decision-making methods accordingly.
Then it draws a graph for a detailed view. The test Csv file is in the testCSV folder.
Written in C #

Slovenian:
Program za osnovne metode odlocanja. 
Podatke prebere iz CSV datoteke in glede na to izracuna za osnovne metode odlocanja.
Nato e izrise graf za podroben ogled. Testna Csv datotek je v mapi testnaCSV.
Napisano v C#
