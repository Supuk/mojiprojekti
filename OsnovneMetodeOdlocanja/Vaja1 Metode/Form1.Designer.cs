﻿namespace Vaja1_Metode
{
    partial class OsnovneMetodeForma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.labelNavodila = new System.Windows.Forms.Label();
            this.labelIzberi = new System.Windows.Forms.Label();
            this.CSVgumb = new System.Windows.Forms.Button();
            this.dataGridViewCSV = new System.Windows.Forms.DataGridView();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.Potrdigumb = new System.Windows.Forms.Button();
            this.lblOptimist = new System.Windows.Forms.Label();
            this.lblPesimist = new System.Windows.Forms.Label();
            this.lblNajmanjseO = new System.Windows.Forms.Label();
            this.lblLaplace = new System.Windows.Forms.Label();
            this.txtBoxOptimist = new System.Windows.Forms.TextBox();
            this.txtBoxPesimist = new System.Windows.Forms.TextBox();
            this.txtBoxLaplace = new System.Windows.Forms.TextBox();
            this.txtBoxNO = new System.Windows.Forms.TextBox();
            this.grafIzris = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.Izrisgumb = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCSV)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafIzris)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNavodila
            // 
            this.labelNavodila.AutoSize = true;
            this.labelNavodila.Location = new System.Drawing.Point(12, 9);
            this.labelNavodila.Name = "labelNavodila";
            this.labelNavodila.Size = new System.Drawing.Size(779, 17);
            this.labelNavodila.TabIndex = 0;
            this.labelNavodila.Text = "Ta aplikacija prebere CSV datoteko in izračuna osnovne metode odločanja. Po želji" +
    " izriše tudi graf z Hurwitzevim kriterijem.";
            this.labelNavodila.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelIzberi
            // 
            this.labelIzberi.AutoSize = true;
            this.labelIzberi.Location = new System.Drawing.Point(33, 82);
            this.labelIzberi.Name = "labelIzberi";
            this.labelIzberi.Size = new System.Drawing.Size(132, 17);
            this.labelIzberi.TabIndex = 1;
            this.labelIzberi.Text = "Izberi CSV datoteko";
            // 
            // CSVgumb
            // 
            this.CSVgumb.BackColor = System.Drawing.Color.Aqua;
            this.CSVgumb.Location = new System.Drawing.Point(193, 69);
            this.CSVgumb.Name = "CSVgumb";
            this.CSVgumb.Size = new System.Drawing.Size(110, 42);
            this.CSVgumb.TabIndex = 2;
            this.CSVgumb.Text = "CSV datoteka";
            this.CSVgumb.UseVisualStyleBackColor = false;
            this.CSVgumb.Click += new System.EventHandler(this.CSVgumb_Click);
            // 
            // dataGridViewCSV
            // 
            this.dataGridViewCSV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCSV.Location = new System.Drawing.Point(36, 130);
            this.dataGridViewCSV.Name = "dataGridViewCSV";
            this.dataGridViewCSV.RowHeadersWidth = 51;
            this.dataGridViewCSV.RowTemplate.Height = 24;
            this.dataGridViewCSV.Size = new System.Drawing.Size(458, 229);
            this.dataGridViewCSV.TabIndex = 3;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk_1);
            // 
            // Potrdigumb
            // 
            this.Potrdigumb.BackColor = System.Drawing.Color.Cyan;
            this.Potrdigumb.Location = new System.Drawing.Point(347, 375);
            this.Potrdigumb.Name = "Potrdigumb";
            this.Potrdigumb.Size = new System.Drawing.Size(147, 28);
            this.Potrdigumb.TabIndex = 4;
            this.Potrdigumb.Text = "Potrdi izbiro";
            this.Potrdigumb.UseVisualStyleBackColor = false;
            this.Potrdigumb.Click += new System.EventHandler(this.button1_Click);
            // 
            // lblOptimist
            // 
            this.lblOptimist.AutoSize = true;
            this.lblOptimist.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOptimist.Location = new System.Drawing.Point(42, 436);
            this.lblOptimist.Name = "lblOptimist";
            this.lblOptimist.Size = new System.Drawing.Size(82, 20);
            this.lblOptimist.TabIndex = 5;
            this.lblOptimist.Text = "Optimist: ";
            this.lblOptimist.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // lblPesimist
            // 
            this.lblPesimist.AutoSize = true;
            this.lblPesimist.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPesimist.Location = new System.Drawing.Point(42, 476);
            this.lblPesimist.Name = "lblPesimist";
            this.lblPesimist.Size = new System.Drawing.Size(84, 20);
            this.lblPesimist.TabIndex = 6;
            this.lblPesimist.Text = "Pesimist: ";
            // 
            // lblNajmanjseO
            // 
            this.lblNajmanjseO.AutoSize = true;
            this.lblNajmanjseO.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNajmanjseO.Location = new System.Drawing.Point(42, 556);
            this.lblNajmanjseO.Name = "lblNajmanjseO";
            this.lblNajmanjseO.Size = new System.Drawing.Size(191, 20);
            this.lblNajmanjseO.TabIndex = 8;
            this.lblNajmanjseO.Text = "Najmanjše obžalovanje: ";
            // 
            // lblLaplace
            // 
            this.lblLaplace.AutoSize = true;
            this.lblLaplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLaplace.Location = new System.Drawing.Point(42, 518);
            this.lblLaplace.Name = "lblLaplace";
            this.lblLaplace.Size = new System.Drawing.Size(78, 20);
            this.lblLaplace.TabIndex = 7;
            this.lblLaplace.Text = "Laplace: ";
            this.lblLaplace.Click += new System.EventHandler(this.label4_Click);
            // 
            // txtBoxOptimist
            // 
            this.txtBoxOptimist.Location = new System.Drawing.Point(275, 436);
            this.txtBoxOptimist.Name = "txtBoxOptimist";
            this.txtBoxOptimist.Size = new System.Drawing.Size(219, 22);
            this.txtBoxOptimist.TabIndex = 9;
            // 
            // txtBoxPesimist
            // 
            this.txtBoxPesimist.Location = new System.Drawing.Point(275, 474);
            this.txtBoxPesimist.Name = "txtBoxPesimist";
            this.txtBoxPesimist.Size = new System.Drawing.Size(219, 22);
            this.txtBoxPesimist.TabIndex = 10;
            // 
            // txtBoxLaplace
            // 
            this.txtBoxLaplace.Location = new System.Drawing.Point(275, 518);
            this.txtBoxLaplace.Name = "txtBoxLaplace";
            this.txtBoxLaplace.Size = new System.Drawing.Size(219, 22);
            this.txtBoxLaplace.TabIndex = 11;
            // 
            // txtBoxNO
            // 
            this.txtBoxNO.Location = new System.Drawing.Point(275, 556);
            this.txtBoxNO.Name = "txtBoxNO";
            this.txtBoxNO.Size = new System.Drawing.Size(219, 22);
            this.txtBoxNO.TabIndex = 12;
            // 
            // grafIzris
            // 
            chartArea2.Name = "ChartArea1";
            this.grafIzris.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.grafIzris.Legends.Add(legend2);
            this.grafIzris.Location = new System.Drawing.Point(654, 108);
            this.grafIzris.Name = "grafIzris";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Series1";
            this.grafIzris.Series.Add(series2);
            this.grafIzris.Size = new System.Drawing.Size(808, 493);
            this.grafIzris.TabIndex = 13;
            this.grafIzris.Text = "chart1";
            // 
            // Izrisgumb
            // 
            this.Izrisgumb.BackColor = System.Drawing.Color.Cyan;
            this.Izrisgumb.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Izrisgumb.Location = new System.Drawing.Point(677, 58);
            this.Izrisgumb.Name = "Izrisgumb";
            this.Izrisgumb.Size = new System.Drawing.Size(286, 40);
            this.Izrisgumb.TabIndex = 14;
            this.Izrisgumb.Text = "Pritisni gumb za izris grafa";
            this.Izrisgumb.UseVisualStyleBackColor = false;
            this.Izrisgumb.Click += new System.EventHandler(this.Izrisgumb_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Cyan;
            this.button1.Location = new System.Drawing.Point(1324, 58);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(138, 40);
            this.button1.TabIndex = 15;
            this.button1.Text = "Pokaži izračune";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // OsnovneMetodeForma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1489, 620);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.Izrisgumb);
            this.Controls.Add(this.grafIzris);
            this.Controls.Add(this.txtBoxNO);
            this.Controls.Add(this.txtBoxLaplace);
            this.Controls.Add(this.txtBoxPesimist);
            this.Controls.Add(this.txtBoxOptimist);
            this.Controls.Add(this.lblNajmanjseO);
            this.Controls.Add(this.lblLaplace);
            this.Controls.Add(this.lblPesimist);
            this.Controls.Add(this.lblOptimist);
            this.Controls.Add(this.Potrdigumb);
            this.Controls.Add(this.dataGridViewCSV);
            this.Controls.Add(this.CSVgumb);
            this.Controls.Add(this.labelIzberi);
            this.Controls.Add(this.labelNavodila);
            this.Name = "OsnovneMetodeForma";
            this.Text = "Osnovne Metode";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCSV)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grafIzris)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNavodila;
        private System.Windows.Forms.Label labelIzberi;
        private System.Windows.Forms.Button CSVgumb;
        private System.Windows.Forms.DataGridView dataGridViewCSV;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button Potrdigumb;
        private System.Windows.Forms.Label lblOptimist;
        private System.Windows.Forms.Label lblPesimist;
        private System.Windows.Forms.Label lblNajmanjseO;
        private System.Windows.Forms.Label lblLaplace;
        private System.Windows.Forms.TextBox txtBoxOptimist;
        private System.Windows.Forms.TextBox txtBoxPesimist;
        private System.Windows.Forms.TextBox txtBoxLaplace;
        private System.Windows.Forms.TextBox txtBoxNO;
        private System.Windows.Forms.DataVisualization.Charting.Chart grafIzris;
        private System.Windows.Forms.Button Izrisgumb;
        private System.Windows.Forms.Button button1;
    }
}

