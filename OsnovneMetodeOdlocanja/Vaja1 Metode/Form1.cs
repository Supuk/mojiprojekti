﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Linq;

namespace Vaja1_Metode
{
    public partial class OsnovneMetodeForma : Form
    {
        DataTable moja_Baza = new DataTable();
        DataTable Hurwitz = new DataTable();

        public OsnovneMetodeForma()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void CSVgumb_Click(object sender, EventArgs e)
        {

           
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if(openFileDialog1.ShowDialog()==System.Windows.Forms.DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;
               // MessageBox.Show(filename);

                ShraninPrikaziCSV(filename);
                labelIzberi.Text = "Uspešno si naložil CSV";
            }
        }

        private void ShraninPrikaziCSV(string filename)
        {
            string[] celi_text = System.IO.File.ReadAllLines(filename);
            string[] data_col = null;
            int x = 0;

            foreach(string text_line in celi_text)
            {
                data_col = text_line.Split(',');

                if (x == 0)
                {
                      // header
                      for(int i=0;i<=data_col.Count()-1; i++)
                    {
                        moja_Baza.Columns.Add(data_col[i]);
                    }
                    x++;
                }
                else
                {
                    moja_Baza.Rows.Add(data_col);
                    //podatki

                }

            }
            dataGridViewCSV.DataSource = moja_Baza;
            this.Controls.Add(dataGridViewCSV);
        }

        private void openFileDialog2_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void openFileDialog1_FileOk_1(object sender, CancelEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //PRVO PESIMIST
            int pesimist = 0;
            int column = 0;
       
            for (int i=1; i<moja_Baza.Columns.Count; i++)
            {
                if (i == 1)
                {
                    int x = Int32.Parse(moja_Baza.Rows[0][i].ToString());
                    int y = Int32.Parse(moja_Baza.Rows[1][i].ToString());
                        if (x > y)
                    {
                        pesimist = y;
                    }
                    else
                    {
                        pesimist = x;
                    }
                    column =i;

                }
                else
                {
                    int pesimist1 = 0;
                    int x = Int32.Parse(moja_Baza.Rows[0][i].ToString());
                    int y = Int32.Parse(moja_Baza.Rows[1][i].ToString());
                        if (x > y)
                    {
                        pesimist1 = y;
                    }
                    else
                    {
                        pesimist1 = x;
                    }
                    if (pesimist > pesimist1)
                    {
                        pesimist = pesimist1;
                    }
                   

                }
               

            }
            string pesimistString = moja_Baza.Columns[column].ToString() + " "+ "("+pesimist.ToString()+")";
           
            txtBoxPesimist.Text = pesimistString;

            //POL OPTIMIST isti princip kot pesimist samo da se pomaknem eno vrsto dol na pozitivni izid
            int optimist = 0;
            int columnOp = 0;
            for (int i = 1; i < moja_Baza.Columns.Count; i++)
            {
                if (i == 1)
                {
                    optimist = Int32.Parse(moja_Baza.Rows[1][i].ToString());
                    columnOp = i;
                }
                else
                {
                    if (optimist < (Int32.Parse(moja_Baza.Rows[1][i].ToString())))
                    {
                        optimist = Int32.Parse(moja_Baza.Rows[1][i].ToString());
                        columnOp = i;
                    }
                }

            }
            string optimistString = moja_Baza.Columns[columnOp].ToString() + " " + "(" + optimist.ToString() + ")";

            txtBoxOptimist.Text = optimistString;


            //LAPLACE

            int laplace = 0;
            int columnLap = 0;

            for (int i = 1; i < moja_Baza.Columns.Count; i++)
            {
                
                 int prvavrednost = Int32.Parse(moja_Baza.Rows[0][i].ToString());
                 int drugavrednost = Int32.Parse(moja_Baza.Rows[1][i].ToString());
                if (i == 1)
                {
                    laplace = (Povprecje(prvavrednost, drugavrednost));
                    columnLap = i;
                }
                else
                {
                    if(laplace< (Povprecje(prvavrednost, drugavrednost)))
                    {
                        laplace= (Povprecje(prvavrednost, drugavrednost));
                        columnLap = i;
                    }
                }
            }

            string LaplaceString = moja_Baza.Columns[columnLap].ToString() + " " + "(" + laplace.ToString() + ")";

            txtBoxLaplace.Text = LaplaceString;

            //SAVAGE NAJMANJSE OBZALOVANJE

            int savage = 0;
            int columnSavage = 0;

            for (int i = 1; i < moja_Baza.Columns.Count; i++)
            {

                int prvavrednostPesimist = Int32.Parse(moja_Baza.Rows[0][i].ToString());
                int drugavrednostOptimist = Int32.Parse(moja_Baza.Rows[1][i].ToString());
                if (i == 1)
                {
                    savage = (IzracunSavage(prvavrednostPesimist, drugavrednostOptimist, pesimist,optimist));
                    columnSavage = i;
                }
                else
                {
                    if (savage > (IzracunSavage(prvavrednostPesimist, drugavrednostOptimist, pesimist, optimist)))
                    {
                        savage = (IzracunSavage(prvavrednostPesimist, drugavrednostOptimist, pesimist, optimist));
                        columnSavage = i;
                    }
                }
            }

            string SavageString = moja_Baza.Columns[columnSavage].ToString() + " " + "(" + savage.ToString() + ")";
            txtBoxNO.Text = SavageString;

            //TO gre skoz vsa števila
            // for (int z = 1; z < moja_Baza.Columns.Count; z++)
            // {
            //     for (int i = 0; i < moja_Baza.Rows.Count; i++)
            //     {
            //         string name = moja_Baza.Rows[i][z].ToString();




            //MessageBox.Show(name);
            //    }
            // }

        }

        private int IzracunSavage(int prvavrednostPesimist, int drugavrednostOptimist, int pesimist, int optimist)
        {
            int zgornji = pesimist - prvavrednostPesimist;
            int spodnji = optimist - drugavrednostOptimist;
            if (zgornji < spodnji)
            {
                return spodnji;
            }
            else
            {
                return zgornji;
            }
        }

        private int Povprecje(int prvavrednost, int drugavrednost)
        {
            int povprecje = (prvavrednost + drugavrednost) / 2;
            return povprecje;
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void Izrisgumb_Click(object sender, EventArgs e)
        {
            var graf = grafIzris.ChartAreas[0];
            graf.AxisX.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            graf.AxisX.LabelStyle.Format = "";
            graf.AxisY.LabelStyle.Format = "";
            graf.AxisX.LabelStyle.IsEndLabelVisible = true;
            graf.AxisY.LabelStyle.IsEndLabelVisible = true;
            
            graf.AxisX.Minimum = 0;
            graf.AxisY.Minimum = 0;
            graf.AxisX.Maximum = 1;

            graf.AxisX.Interval=0.1;
            graf.AxisY.Interval = 5;
            graf.AxisX.Title = "h";
            graf.AxisY.Title = "vrednost alternativ";

            grafIzris.Series[0].IsVisibleInLegend = false;

            for (int i = 1; i < moja_Baza.Columns.Count; i++)
            {

                grafIzris.Series.Add(moja_Baza.Columns[i].ToString());
                grafIzris.Series[(moja_Baza.Columns[i].ToString())].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                // grafIzris.Series(moja_Baza.Columns[i].ToString()].Color = Color.Blue;

                
                double prvaVrednost = double.Parse(moja_Baza.Rows[0][i].ToString());
                double drugaVrednost = double.Parse(moja_Baza.Rows[1][i].ToString());
                double p=IzracunPremika(prvaVrednost, drugaVrednost);

                

                grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0, prvaVrednost);
               // grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.1, (prvaVrednost + (p)) );
                //grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.2, (prvaVrednost + (2*p)));
                //grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.3, (prvaVrednost + (3 * p)));
                //grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.4, (prvaVrednost + (4 * p)));
                //grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.5, (prvaVrednost + (5 * p)));
                //grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.6, (prvaVrednost + (6 * p)));
                //grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.7, (prvaVrednost + (7 * p)));
                //grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.8, (prvaVrednost + (8 * p)));
                //grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(0.9, (prvaVrednost + (9 * p)));
                grafIzris.Series[(moja_Baza.Columns[i].ToString())].Points.AddXY(1,  drugaVrednost);
            }


        }

        private double IzracunPremika(double prvaVrednost, double drugaVrednost)
        { double x = 0;
            double y = drugaVrednost - prvaVrednost;
            x = y / 10;
            return x;

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            // List<List<String>> matrix = new List<List<String>>(); //Creates new nested List
            // matrix.Add(new List<String>()); //Adds new sub List
            //  for (int i = 1; i < moja_Baza.Columns.Count; i++)
            // {
            //    List<string> list1 = new List<string>();
            //    matrix.Add(list1);
            // }
            // matrix[0].Add("h");
            // matrix[0].Add("0");
            //  matrix[0].Add("0.1");
            //  matrix[0].Add("0.2");
            //  matrix[0].Add("0.3");
            // matrix[0].Add("0.4");
            // matrix[0].Add("0.5");
            //matrix[0].Add("0.6");
            //matrix[0].Add("0.7");
            //matrix[0].Add("0.8");
            //matrix[0].Add("0.9");
            // matrix[0].Add("1");

            string[] data_col = new String[moja_Baza.Columns.Count+1];
            //data_col[0] = "h";
            //Hurwitz.Columns.Add(data_col[0]);
            for (int i = 0; i < moja_Baza.Columns.Count; i++)
            {
                if (i == 0)
                {
                    data_col[0] = "h";
                }
                else
                {

                    data_col[i] = moja_Baza.Columns[i].ToString();
                }
                Hurwitz.Columns.Add(data_col[i]);
            }

            string[] prvo = new String[moja_Baza.Columns.Count ];    prvo[0] = "0.1";
            string[] drugo = new String[moja_Baza.Columns.Count ];   drugo[0] = "0.2";
            string[] tretje = new String[moja_Baza.Columns.Count ];  tretje[0] = "0.3";
            string[] cetrto = new String[moja_Baza.Columns.Count ];  cetrto[0] = "0.4";
            string[] peto = new String[moja_Baza.Columns.Count ];    peto[0] = "0.5";
            string[] sesto = new String[moja_Baza.Columns.Count ];   sesto[0] = "0.6";
            string[] sedmo = new String[moja_Baza.Columns.Count ];   sedmo[0] = "0.7";
            string[] osmo = new String[moja_Baza.Columns.Count ];    osmo[0] = "0.8";
            string[] deveto = new String[moja_Baza.Columns.Count ];  deveto[0] = "0.9";
            string[] deseto = new String[moja_Baza.Columns.Count ];  deseto[0] = "1";
            string[] pPrvo = new String[moja_Baza.Columns.Count ];   pPrvo[0] = "0";
           

            for (int i = 1; i < moja_Baza.Columns.Count; i++)
            {


                double prvaVrednost = double.Parse(moja_Baza.Rows[0][i].ToString());
                double drugaVrednost = double.Parse(moja_Baza.Rows[1][i].ToString());

                double p =  IzracunPremika(prvaVrednost, drugaVrednost);
                double prvaSt = prvaVrednost + p;
                double drugaSt = prvaVrednost + p+p;
                double tretjaSt = prvaVrednost + p+p+p;
                double cetrtaSt = prvaVrednost + p+p+p+p;
                double petaSt = prvaVrednost + p+p+p+p+p; 
                double sestaSt = prvaVrednost + p+p+p+p+p+p;
                double sedmaSt = prvaVrednost + p+p+p+p+p+p+p;
                double osmaSt = prvaVrednost + p+p+p+p+p+p+p+p;
                double devetaSt = prvaVrednost + p+p+p+p+p+p+p+p+p;


                
                pPrvo[i]= prvaVrednost.ToString();
                prvo[i] = ( prvaSt.ToString());
                drugo[i] = (drugaSt.ToString());
                tretje [i] = (tretjaSt.ToString());
                cetrto[i] = (cetrtaSt.ToString());
               peto[i] = (petaSt.ToString());
               sesto[i] = (sestaSt.ToString());
                sedmo[i] = (sedmaSt.ToString());
                osmo [i] = (osmaSt.ToString());
                deveto[i] = (devetaSt.ToString());
                deseto [i] =(moja_Baza.Rows[1][i].ToString());

            }

            Hurwitz.Rows.Add(pPrvo);
            Hurwitz.Rows.Add(prvo);
            Hurwitz.Rows.Add(drugo);
            Hurwitz.Rows.Add(tretje);
            Hurwitz.Rows.Add(cetrto);
            Hurwitz.Rows.Add(peto);
            Hurwitz.Rows.Add(sesto);
            Hurwitz.Rows.Add(sedmo);
            Hurwitz.Rows.Add(osmo);
            Hurwitz.Rows.Add(deveto);
            Hurwitz.Rows.Add(deseto);

            dataGridViewCSV.DataSource = Hurwitz;

        }
    }
    }

