﻿using System;
using System.Collections.Generic;

namespace Vigenerjeva_šifra
{
    class Vsifra
    {
        const int lettersS = 33;
        const int lettersEN = 26;
        char[] polje = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        private List<char> temp = null;
        private List<string> besedoSif = new List<string>();
        private List<string> besedoDesifr = new List<string>();
        private int letters = 0;
        private bool upper = false;
        private string kljuc = string.Empty;

        public Vsifra(string kljuc)
        {
            this.kljuc = kljuc;
        }

        public string getKljuc(int len)
        {
            string res = string.Empty;
            int j = 0;

            for (int i = 0; i < len; ++i)
            {
                if (j == this.kljuc.Length)
                    j = 0;

                res += this.kljuc[j++];
            }

            return res;
        }

        public void initAlphabet(char ch)
        {
            if (char.IsUpper(ch))
            {
                this.upper = true;
                ch = char.ToLower(ch);
            }
            else
                this.upper = false;

            if (Form1.alphabetS.Contains(ch))
            {
                this.temp = Form1.alphabetS;
                this.letters = lettersS;
            }
            else if (Form1.alphabetEN.Contains(ch))
            {
                this.temp = Form1.alphabetEN;
                this.letters = lettersEN;
            }
            else
                this.letters = 0;
        }

        public string Encrypt(string text)
        {
            string res = string.Empty;
            string kljuc = this.getKljuc(text.Length);
            int iKey = 0;

            for (int i = 0; i < text.Length; ++i)
            {
                initAlphabet(kljuc[i]);
                iKey = this.temp.IndexOf(char.ToLower(kljuc[i]));

                initAlphabet(text[i]);
                if (this.letters != 0)
                {
                    int encryptChar = (this.temp.IndexOf(char.ToLower(text[i])) + iKey) % this.letters;

                    if (this.upper)
                        res += char.ToUpper(this.temp[encryptChar]);
                    else
                        res += this.temp[encryptChar];
                }
                else
                    res += text[i];
            }

            // encryptStevila(res);
            return encryptStevila(res); 
        }

        private string encryptStevila(string res)
        {
            
            int kljucDolzina = this.kljuc.Length;

            for (int i = 0; i < res.Length; ++i)
            {
                if (res[i] == '0' || res[i] == '1' || res[i] == '2' || res[i] == '3' || res[i] == '4' || res[i] == '5' || res[i] == '6' || res[i] == '7' || res[i] == '8' || res[i] == '9')
                {
                    char crka = '0';
                    int index = 0;
                    for (int j = 0; j < polje.Length; j++)
                    {
                        if (res[i] == polje[j])
                        {
                            crka = polje[j];
                            index = j;
                        }

                    }
                    int x = index + kljucDolzina;
                    if (x > polje.Length)
                    {
                        x = x - polje.Length;

                    }
                    besedoSif.Add(polje[x].ToString());




                }
                else
                    besedoSif.Add(res[i].ToString());
            }

            string rez = "";
            foreach (var i in besedoSif)
            {
                rez = rez + i;
            }



            return rez;

        }

        public string Decrypt(string text)
        {
           
            string res = string.Empty;
            string kljuc = this.getKljuc(text.Length);
            int iKey = 0;

            for (int i = 0; i < text.Length; ++i)
            {
                initAlphabet(kljuc[i]);
                iKey = this.temp.IndexOf(char.ToLower(kljuc[i]));

                initAlphabet(text[i]);
                if (this.letters != 0)
                {
                    int decryptChar = (this.temp.IndexOf(char.ToLower(text[i])) - iKey);
                    decryptChar = (decryptChar % this.letters + this.letters) % this.letters;

                    if (this.upper)
                        res += char.ToUpper(this.temp[decryptChar]);
                    else
                        res += this.temp[decryptChar];
                }
                else
                    res += text[i];
            }

            //return res;
            return decryptStevila(res);
          
        }

        private string decryptStevila(string res)
        {
          
            int kljucDolzina = this.kljuc.Length;
            for (int i = 0; i < res.Length; ++i)
            {
                if (res[i] == '0' || res[i] == '1' || res[i] == '2' || res[i] == '3' || res[i] == '4' || res[i] == '5' || res[i] == '6' || res[i] == '7' || res[i] == '8' || res[i] == '9')
                {
                    char crka = '0';
                    int index = 0;
                    for (int j = 0; j < polje.Length; j++)
                    {
                        if (res[i] == polje[j])
                        {
                            crka = polje[j];
                            index = j;
                        }

                    }
                    int x = index - kljucDolzina;
                    if (x < 0)
                    {
                        x = Math.Abs(x);

                    }
                    besedoDesifr.Add(polje[x].ToString());




                }
                else
                    besedoDesifr.Add(res[i].ToString());
            }

            string rez = "";
            foreach (var i in besedoDesifr)
            {
                rez = rez + i;
            }



            return rez;

        }

    }
}

