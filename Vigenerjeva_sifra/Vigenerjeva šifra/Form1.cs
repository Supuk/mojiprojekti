﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;


namespace Vigenerjeva_šifra
{
    
    public partial class Form1 : Form
    {
        public static List<char> alphabetS = new List<char>();
        public static List<char> alphabetEN = new List<char>();
        public Form1()
        {
            InitializeComponent();

            for (int i = 1072; i < 1104; ++i)
            {
                if (i == 1078)
                    alphabetS.Add(Convert.ToChar(1105));

                alphabetS.Add(Convert.ToChar(i));
            }

            for (int i = 97; i < 123; ++i)
                alphabetEN.Add(Convert.ToChar(i));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UTF8Encoding utf8 = new UTF8Encoding();

            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;
                // MessageBox.Show(filename);

                string celo_besedilo = System.IO.File.ReadAllText(filename);
                Byte[] encodedBytes = utf8.GetBytes(celo_besedilo);
                for (int ctr = 0; ctr < encodedBytes.Length; ctr++)
                {
                    Console.Write("{0:X2} ", encodedBytes[ctr]);
                    if ((ctr + 1) % 25 == 0)
                        Console.WriteLine();
                }
                String decodedString = utf8.GetString(encodedBytes);
                textBox1.Text = decodedString;

            }
        }
        string celi_text;
       

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            
            celi_text = textBox1.Text;
            string rez = string.Empty;

            Vsifra vs = new Vsifra(txtBoxKljuc.Text);
            rez = vs.Encrypt(celi_text);

            textBox2.Text = rez;

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            string rez = string.Empty;
            Vsifra vs = new Vsifra(txtBoxKljuc.Text);
            rez = vs.Decrypt(textBox2.Text);

            textBox1.Text = rez;

        }
    }
}
