English:
This is a program written in C # in which we encrypt text using the Vigener cipher.
We also enter the key on the basis of which we encrypt the text.
If we do not enter anything, it takes the default abdu.
The test text for the test is in the Test Text folder. However, you can enter any text.
Slovenian:
To je program napisan v C# pri katerem s pomocjo Vigenerjeve sifre sifriramo besedilo.
Vnesemo tudi kljuc na podlagi katerega sifriramo besedilo. 
V kolikor ne vnesemo nic vzame default abdu.
Testno besedilo za preizkus je v mapi testnoBesedilo. Lahko pa vnesemo poljubno besedilo.
